import {
  View
} from 'react-native';
import React, { Component, useEffect } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { NavigationContainer } from '@react-navigation/native';

import RootReducer from './src/redux/RootReducer'
import AppNavigator from './src/navigator/AppNavigator';
import SplashScreen from 'react-native-bootsplash';

/** Register global state to redux store */
const store = createStore(RootReducer, applyMiddleware(thunk));

const App = () => {
  useEffect(() => {
    SplashScreen.hide()
  }, [])
  return(
    <Provider store={store} >
            <NavigationContainer>
              <AppNavigator />
            </NavigationContainer>
              
          </Provider>
  )
}

export default App
