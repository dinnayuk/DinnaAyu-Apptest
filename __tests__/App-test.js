import React from "react";
import renderer from "react-test-renderer";
import App from "../App"
import SplashScreen from 'react-native-bootsplash'

jest.mock('react-native-bootsplash', () => {
    return { 
      hide: jest.fn(),
      show: jest.fn()
    };
  });

  jest.fn().mockImplementation(() => {
    return {
       Modal: jest.fn()
     }
 });

describe("<App />", () => {
    it('has 1 child', () => {
        const tree = renderer.create(<App />).toJSON();
        expect(tree.children.length).toBe(1);
    });
});
