import React from "react";
import renderer from "react-test-renderer";
import ContactList from "../src/screens/ContactList"
import { useSelector, useDispatch } from 'react-redux'; 

const mockDispatch = jest.fn();
const mockNavigate = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
  navigation: () => ({
    navigate: mockNavigate,
  }),
}));

it('renders correctly', () => {
    const tree = renderer.create(<ContactList />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  
