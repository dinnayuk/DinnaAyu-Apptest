import axios from "axios";
import { Alert } from "react-native";

const BASE_URL = 'https://simple-contact-crud.herokuapp.com'

export const CONTACT_REDUX = {
    IS_LOADING: 'IS_LOADING',
    CONTACT_LIST: 'CONTACT_LIST'
}

function updateContactList(list){
    return ({
        type: CONTACT_REDUX.CONTACT_LIST,
        data: list
    })
}

export function isLoading(isLoading){
    return ({
        type: CONTACT_REDUX.IS_LOADING,
        data: isLoading
    })
}

export function getContacts(){
    const URL = `${BASE_URL}/contact`
    return dispatch => {
        dispatch(isLoading(true))
        return axios.get(URL)
        .then(response => {
            const {data} = response
            if (data.data && data.data.length > 0){
                dispatch(updateContactList(data.data))
                dispatch(isLoading(false))
            }
        })
        .catch(error => {
            dispatch(isLoading(false))
        })
    };

}

export function saveContact(params, callback){
    const URL = `${BASE_URL}/contact`
    return dispatch => {
        dispatch(isLoading(true))
        return axios.post(URL, params)
        .then(response => {
            const {data} = response
            if (data && data.message && data.message.toLowerCase() === "contact saved"){
                Alert.alert(
                    'Success!',
                    `${data.message}`,
                    [
                      {text: 'OK', onPress: () => {
                        callback()
                      }},
                    ],
                    { cancelable: false }
                )
            }
        })
        .catch(error => {
            const {response} = error
            const {data} = response
            Alert.alert(`${data.message}`)
            dispatch(isLoading(false))
        })
    };
}

export function deleteContact(id, callback){
    const URL = `${BASE_URL}/contact/${id}`

    return dispatch => {
        dispatch(isLoading(true))
        return axios.delete(URL)
        .then(response => {
            const {data} = response
            if (data && data.message && data.message.toLowerCase() === "contact deleted"){
                Alert.alert(
                    'Success!',
                    `${data.message}`,
                    [
                      {text: 'OK', onPress: () => {
                        callback()
                      }},
                    ],
                    { cancelable: false }
                )
            }
        })
        .catch(error => {
            const {response} = error
            const {data} = response
            Alert.alert(`${data.message}`)
            dispatch(isLoading(false))
        })
    };
}

export function editContact(id, param, callback){
    const URL = `${BASE_URL}/contact/${id}`

    return dispatch => {
        dispatch(isLoading(true))
        return axios.put(URL, param)
        .then(response => {
            const {data} = response
            if (data && data.message && data.message.toLowerCase() === "contact edited"){
                Alert.alert(
                    'Success!',
                    `${data.message}`,
                    [
                      {text: 'OK', onPress: () => {
                        callback()
                      }},
                    ],
                    { cancelable: false }
                )
            }
        })
        .catch(error => {
            const {response} = error
            const {data} = response
            Alert.alert(`${data.message}`)
            dispatch(isLoading(false))
        })
    };
}