import { CONTACT_REDUX } from "../actions/ContactAction";

const initialState = {
    isLoading: false,
    contactList: []
}

const Reducer = (state = initialState, action) => {
    if (action.type === CONTACT_REDUX.CONTACT_LIST){
        return { ...state, contactList: action.data};
    } else if (action.type === CONTACT_REDUX.IS_LOADING){
        return {...state, isLoading: action.data}
    }

    return state;
}

export default Reducer;
