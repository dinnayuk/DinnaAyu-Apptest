import { combineReducers } from 'redux';
import ContactReducer from './reducers/ContactReducer';

/** Combine all available reducers which will be accessible from all components */
export default combineReducers({
    contact: ContactReducer
});
