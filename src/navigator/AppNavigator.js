import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ContactList from '../screens/ContactList'
import AddContact from '../screens/AddContact';
import ContactDetail from '../screens/ContactDetail';
import { TouchableOpacity } from 'react-native-gesture-handler';
import IonIcons from 'react-native-vector-icons/Ionicons'
import { Button, Text } from 'react-native';

export default  AppNavigator = () => {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="ContactList" component={ContactList} options={({route, navigation}) => {
                return {
                    title: 'Contacts',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#F79489'
                    },
                    headerTitleAlign: 'left',
                    headerRightContainerStyle:{paddingRight: 12},
                    headerRight: () => (
                        <TouchableOpacity onPress={() => navigation.navigate('AddContact')}>
                            <IonIcons name='add' size={30} color="#F79489" />
                        </TouchableOpacity>
                      )
                }
            }}/>
            <Stack.Screen name="AddContact" component={AddContact} options={({route, navigation}) => {
                const title = route?.params?.data ? "Edit Contact" : 'Add Contact'
                return {
                title: `${title}`,
                headerTitleStyle: {
                    fontWeight: '400',
                    fontSize: 16,
                    color: '#F79489'
                }}
            }}/>
            <Stack.Screen name="ContactDetail" component={ContactDetail}
                options={({route, navigation}) => {
                    const {params} = route
                    const {data} = params
                    return {
                        title: `${data.value}`,
                        headerTitleStyle: {
                            fontWeight: '500',
                            fontSize: 20,
                            color: '#F79489'
                        },
                        headerTitleAlign: 'center'
                    }
                }}
            />
        </Stack.Navigator>
    );
}
