import React from 'react'
import { Modal, View } from 'react-native';
import AwesomeLoading from 'react-native-awesome-loading';

const Loading = () => {
    return (
        <Modal transparent style={{flex: 1}}>
            <AwesomeLoading indicatorId={8} size={50} isActive={true} text="loading" />
        </Modal>
    )
}

export default Loading