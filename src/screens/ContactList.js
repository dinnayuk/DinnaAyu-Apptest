import React, { useEffect, useState } from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, RefreshControl} from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { AlphabetList } from "react-native-section-alphabet-list";
import IonIcons from 'react-native-vector-icons/Ionicons'

import { getContacts } from '../redux/actions/ContactAction';
import { useMemo } from 'react';
import Loading from '../components/loading';
import { ScrollView } from 'react-native-gesture-handler';

const ContactList = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation()

    let {contact} = useSelector(state => state)

    let [contacts, setContacts] = useState([])
    let [isRefresh, setIsRefresh] = useState(false)
    const sortContacts = useMemo(() => doSortContacts(contacts), [contacts]);

    useEffect(() => {
        doContactsAPI()
    }, [])

    useEffect(() => {
        if (contact.contactList.length > 0){
            setContacts(contact.contactList)
        }

    }, [contact.contactList])

    function doContactsAPI(){
        dispatch(getContacts())
    }

    function doSortContacts(list){
        const sorted = list.sort((prev, next) => (prev.firstName > next.firstName) ? 1 : ((next.firstName > prev.firstName) ? -1 : 0))
        var result = []

        sorted.map((item, index) => {
            item['value'] = `${item.firstName} ${item.lastName}`
            item['key'] = index
            result.push(item)
        })
        return result
    }
    function renderList(){
        return (
            <AlphabetList
                data={sortContacts}
                indexLetterStyle={{ 
                    color: '#F8AFA6',
                    fontWeight: '400',
                    fontSize: 16
                }}
                uncategorizedAtTop
                style={{paddingRight: 30, backgroundColor: 'white', paddingBottom: 20}}
                indexContainerStyle={{width: 30, alignItems: 'flex-end'}}
                indexLetterContainerStyle={{width: 30, height: 30, paddingRight: 8, marginBottom: 12, alignItems: 'flex-end'}}
                renderCustomItem={(item) => {
                    return(
                    <View style={{paddingLeft: 16}}>
                        <TouchableOpacity style={Styles.row} onPress={() => navigation.navigate("ContactDetail", {data: item})}>
                            <IonIcons name="person-circle" size={70} color="white" />
                            <Text style={Styles.contactName}>{item.value}</Text>
                        </TouchableOpacity>
                    </View>
                )}}
                listHeaderHeight={50}
                renderCustomSectionHeader={section => {
                    return (<View style={Styles.sectionHeaderContainer}>
                        <Text style={Styles.sectionHeaderLabel}>{section.title}</Text>
                    </View>)
                }}
            />
        )
    }

    return (
        <ScrollView style={Styles.container}
        refreshControl={
            <RefreshControl refreshing={isRefresh} onRefresh={() => {
                setIsRefresh(true)
                dispatch(getContacts())
                setTimeout(() => {
                    setIsRefresh(false)
                  }, 1000)
            }}/>
        }>
            {sortContacts.length > 0 ? renderList() : null}
            {contact.isLoading ? <Loading /> : null}
        </ScrollView>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        padding: 12,
        backgroundColor: '#FADCD9'
    },
    contactName: {
        fontSize: 16,
        fontWeight: '400',
        color: '#2E2E2E',
        marginLeft: 12
    },
    sectionHeaderContainer: {
        height: 70,
        backgroundColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 16,
      },
    
      sectionHeaderLabel: {
        color: '#F8AFA6',
        fontSize: 24,
        fontWeight: '500'
      }
})

export default ContactList