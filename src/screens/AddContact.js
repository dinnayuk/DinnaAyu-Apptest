import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import {View, Text, StyleSheet, Modal, Image, TextInput, ScrollView} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import IonIcons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../components/loading';
import { editContact, getContacts, saveContact } from '../redux/actions/ContactAction';

const AddContact = ({route, navigation}) => {
    const dispatch = useDispatch()
    let {contact} = useSelector(state => state)

    let [isShowOptions, setIsShowOptions] = useState(false)
    let [selectedOption, setSelectionOption] = useState(-1)
    let [photoSource, setPhotoSource] = useState("N\A")
    let [firstName, setFirstName] = useState("")
    let [lastName, setLastName] = useState("")
    let [age, setAge] = useState("")
    let [id, setId] = useState("")
    let [isEdit, setIsEdit] = useState(false)

    let firstNameInput = useRef()
    let lastNameInput = useRef()
    let ageInput = useRef()

    useEffect(() => {
        const {params} = route;
        if (params){
            const {data} = params
            if (data){
                setIsEdit(true)
                setFirstName(data.firstName)
                setLastName(data.lastName)
                setAge(`${data.age}`)
                setPhotoSource(data.photo)
                setId(data.id)
            }
        }
    }, [])

    function openAction(){
        setIsShowOptions(false)
        setSelectionOption(-1)
        if (selectedOption === 1){
            openCamera()
        } else if (selectedOption === 2){
            openGallery()
        }
    }

    function openCamera(){
        {
            let options = {
              storageOptions: {
                skipBackup: true,
                path: 'images',
              },
            };
            launchCamera(options, (response) => {
              console.log('Response = ', response);
        
              if (response.didCancel) {
                console.log('User cancelled image picker');
              } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
              } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
              } else {
                setPhotoSource(response.assets[0].uri)
              }
            });
        
          }
    }

    function openGallery(){
        let options = {
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
      
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
              alert(response.customButton);
            } else {
                setPhotoSource(response.assets[0].uri)
            }
          });
    }

    function optionGetPhoto(){
        return (
            <Modal
                onDismiss={() => setIsShowOptions(false)}
                onRequestClose={() => setIsShowOptions(false)}
                visible={isShowOptions}
                animationType='slide'
                transparent={true}>
                <TouchableOpacity style={{height: '100%', width: '100%', alignItems: 'flex-end', justifyContent: 'flex-end'}}
                    onPress={() => setIsShowOptions(false)}
                >
                    <View style={Styles.bottomOptionsWrapper}>
                        <View style={{flexDirection: 'row', width: '100%', marginTop: 24}}>
                            <View style={Styles.iconWrapper}>
                                <MaterialCommunityIcons.Button name='camera' size={50} color="#EED6D3"
                                    onPress={() => setSelectionOption(1)}
                                    iconStyle={{marginRight: 0}}
                                    borderRadius={50}
                                    activeOpacity={1}
                                    style={Styles.optionIcon(selectedOption === 1)}
                                />
                            </View>

                            <View style={Styles.iconWrapper}>
                                <MaterialCommunityIcons.Button name='panorama' size={50} color="#EED6D3"
                                    onPress={() => setSelectionOption(2)}
                                    iconStyle={{marginRight: 0}}
                                    borderRadius={50}
                                    activeOpacity={1}
                                    style={Styles.optionIcon(selectedOption === 2)}
                                />
                            </View>
                        </View>

                        <View style={{flexDirection: 'row', width: '100%'}}>
                            <Text onPress={() => setIsShowOptions(false)} style={Styles.optionAction(true)}>Cancel</Text>
                            <Text onPress={() => openAction()} style={Styles.optionAction(selectedOption !== -1)}>Open</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Modal>
        )
    }

    function contactInfo(){
        return (
            <View style={{width: '95%', alignSelf: 'center'}}  >
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 12, backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={{...Styles.textInputTitle}}>First Name</Text>
                    <TextInput
                        ref={firstNameInput}
                        value={firstName}
                        placeholder='Enter the first name'
                        placeholderTextColor={'#F9F1F0'}
                        style={Styles.textInput}
                        onChangeText={text => setFirstName(text)}
                        returnKeyType="next"
                        maxLength={50}
                        onSubmitEditing={() => {
                            if (lastNameInput){
                                lastNameInput.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 12, backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={{...Styles.textInputTitle}}>Last Name</Text>

                    <TextInput
                        ref={lastNameInput}
                        value={lastName}
                        placeholder='Enter the last name'
                        placeholderTextColor={'#F9F1F0'}
                        style={Styles.textInput}
                        onChangeText={text => setLastName(text)}
                        returnKeyType="next"
                        maxLength={50}
                        onSubmitEditing={() => {
                            if (ageInput){
                                ageInput.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={{...Styles.textInputTitle}}>Age</Text>
                    <TextInput
                        ref={ageInput}
                        value={age}
                        placeholder='Enter the age'
                        placeholderTextColor={'#F9F1F0'}
                        style={Styles.textInput}
                        keyboardType='numeric'
                        maxLength={3}
                        onChangeText={text => setAge(text)}
                        returnKeyType="done"
                    />
                </View>
            </View>
        )
    }

    function goBack(){
        navigation.goBack()
        dispatch(getContacts())
    }

    function editCallback(){
        navigation.pop(2)
        dispatch(getContacts())
    }

    function doSave(){
        const params = {
            "firstName": firstName,
            "lastName": lastName,
            "age": age,
            "photo": photoSource
        }
        if (isEdit){
            dispatch(editContact(id, params, editCallback))
        } else {
            dispatch(saveContact(params, goBack))
        }
        
    }

    function isDisable(){
        return !firstName || !lastName || !age
    }

    return (
        <View style={Styles.container}>
            <ScrollView style={{flex: 1}} contentContainerStyle={{flexGrow: 1, width: '100%', alignItems: 'center'}}>
                <View style={{width: '100%', paddingVertical: 20, justifyContent: 'center', alignItems: 'center'}}>
                    {photoSource !== "N\A" ? <Image source={{uri: photoSource}} style={{width: 100, height: 100, borderRadius: 50, resizeMode: 'cover'}} /> : <TouchableOpacity style={Styles.photoButton} onPress={() => setIsShowOptions(true)}>
                        <IonIcons size={50} color={'white'} name="camera" />
                    </TouchableOpacity>}
                </View>

                {contactInfo()}

                <View style={{width: '95%'}}>
                    <TouchableOpacity disabled={isDisable()} onPress={() => doSave()} style={{marginTop: 50, height: 50, backgroundColor: isDisable() ? '#F9F1F0' : '#EED6D3', justifyContent: 'center', alignItems: 'center', borderRadius: 16}}>
                        <Text style={{fontSize: 16, color: isDisable() ? '#FADCD9' : '#F79489' }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            

            {optionGetPhoto()}

            {contact.isLoading ? <Loading /> : null}
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%', height: '100%',
        backgroundColor: 'white'
    },
    photoButton: {width: 100, height: 100, borderRadius: 50, backgroundColor: '#EED6D3', justifyContent: 'center', alignItems: 'center'},
    bottomOptionsWrapper: {width: '100%',
        backgroundColor: '#F9F1F0',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    iconWrapper: {width: '50%', justifyContent: 'center', alignItems: 'center'},
    optionIcon: isSelected => ({
        backgroundColor: isSelected ? 'white' : '#F9F1F0',
        justifyContent: 'center', alignItems: 'center'}),
    optionAction: isActive =>  ({
        width: '50%',
        textAlign: 'center',
        fontSize: 16,
        color: isActive ? '#F79489' : 'lightgrey',
        paddingVertical: 12
    }),
    textInput: {
        width: '60%',
        fontSize: 16,
        color: '#F79489',
        borderBottomWidth: 1,
        borderBottomColor: 'white'
    },
    textInputTitle: {
        fontSize: 16,
        color: '#F79489',
        width: '40%'
    }
    
})

export default AddContact