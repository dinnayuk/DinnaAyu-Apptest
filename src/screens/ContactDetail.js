import React, { useEffect, useState } from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity, Alert} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useDispatch } from 'react-redux'
import { deleteContact, getContacts } from '../redux/actions/ContactAction'

const ContactDetail = ({route, navigation}) => {
    const dispatch = useDispatch();

    let [photoSrc, setPhotoSrc] = useState("")
    let [firstName, setFirstName] = useState("")
    let [lastName, setLastName] = useState("")
    let [age, setAge] = useState("")
    let [id, setId] = useState("")

    useEffect(() => {
        const {params} = route
        const {data} = params
        if (data){
            setPhotoSrc(data.photo)
            setFirstName(data.firstName)
            setLastName(data.lastName)
            setAge(data.age)
            setId(data.id)
        }
    }, [])

    function contactInfo(){
        return (
            <View style={{width: '95%', alignSelf: 'center', marginTop: 30}}  >
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 12, backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={Styles.textInputTitle}>First Name</Text>
                    <Text style={Styles.textInput}>{firstName}</Text>
                </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 12, backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={Styles.textInputTitle}>last Name</Text>
                    <Text style={Styles.textInput}>{lastName}</Text>
                </View>
                <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', backgroundColor: '#EED6D3', padding: 8, borderRadius: 16}}>
                    <Text style={Styles.textInputTitle}>Age</Text>
                    <Text style={Styles.textInput}>{age}</Text>
                </View>
            </View>
        )
    }

    function footer(){
        return (
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 50}}>
                <TouchableOpacity onPress={() => {
                    Alert.alert(
                        "",
                        `Do you want to delete ${firstName} ${lastName} from contacts?`,
                        [
                          {text: 'OK', onPress: () => {
                            dispatch(deleteContact(id, deteleCallback))
                          }},
                          {text: 'Cancel', onPress: () => {
                            console.log("Cancel")
                          }}
                        ],
                        { cancelable: false }
                    )
                }} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <AntDesign name="delete" size={24} color={'#F79489'} />
                    <Text style={{fontSize: 14, color: '#F79489'}}>Delete</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate("AddContact", {data: route.params.data})} style={{justifyContent: 'center', alignItems: 'center', marginLeft: 20}}>
                    <AntDesign name="edit" size={24} color={'#F79489'} />
                    <Text style={{fontSize: 14, color: '#F79489'}}>Edit</Text>
                </TouchableOpacity>
            </View>
        )
    }

    function deteleCallback(){
        dispatch(getContacts())
        navigation.goBack()
    }

    return (
        <View style={Styles.container}>
            <ScrollView style={{flex: 1}} contentContainerStyle={{flexGrow: 1, width: '100%', alignItems: 'center', paddingTop: 20}}>
                <Image source={{uri: photoSrc}} style={Styles.photo} />
                {contactInfo()}

                {footer()}
            </ScrollView>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    photo: {width: 100, height: 100, borderRadius: 50, resizeMode: 'cover'},
    textInput: {
        width: '60%',
        fontSize: 16,
        color: '#F79489'
    },
    textInputTitle: {
        fontSize: 16,
        color: '#F79489',
        width: '40%'
    }
})
export default ContactDetail